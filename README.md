# Gitlab CI/CD pipeline with AWS for Java project

Script to deploy Java code using gradle onto AWS EC2 using CodeDeploy service.

## Reference for entire setup below:

https://stackoverflow.com/questions/38671818/how-to-deploy-with-gitlab-ci-to-ec2-using-aws-codedeploy-codepipeline-s3

## Enhancement

Script .gitlab-ci.yml has addition scripts for CI/CD setup which is incorrect in the above reference.